import React,{useState} from 'react'
import { View, Text, TextInput,StyleSheet,TouchableOpacity, Alert} from 'react-native'
import List from './components/List'

const data=[
  {id:'1', nombre:'pedro'},
  {id:'2', nombre:'carlos'},
  {id:'3', nombre:'rogrigo'},
  {id:'4', nombre:'antonio'},
]

const App =()=> {
  const [lista, setLista]=useState(data);
  const [nombre, setNombre]=useState('');

  const guardar = () =>{
    if(nombre.trim()){
      const nuevoNombre={
        id:Math.round(Math.random()*10000).toString(),
        nombre
      }
      setLista([...lista, nuevoNombre]);
      setNombre('');
    }else{
      Alert.alert(
        'Error...',
        'El nombre es requerido',
        [
          {text:'OK'}
        ]
      )
    }
  }
  return (
    <View style={styles.container}> 
      <View style={styles.viewAgregar}>
        <TextInput 
            value={nombre}
            style={styles.input} 
            placeholder="Escribe tu nombre"
            onChangeText={(texto)=>setNombre(texto)}
          />
        <TouchableOpacity style={styles.boton} onPress={()=>guardar()}>
          <View >
            <Text style={styles.textoBoton}>Agregar</Text>
          </View>
        </TouchableOpacity>
      </View>
      <List lista={lista} />
    </View>
  )
}
const styles=StyleSheet.create({
  container:{
    flex:1,
    marginHorizontal:10,
  },  
  viewAgregar:{
    flexDirection:'row',
    marginVertical:20,
  },
  input:{
    borderWidth:1,
    borderColor:'#048DE3',
    flex:3,
    color:'#000',
    marginLeft:10,
    paddingHorizontal:10,
  },
  boton:{
    backgroundColor:'#048DE3',
    justifyContent:'center',
    alignItems:'center',
    flex:1,
  },
  textoBoton:{
    color:'white',
    fontSize:15
  }
})
export default App;