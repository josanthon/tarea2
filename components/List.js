import React from 'react'
import { View, Text,StyleSheet,ScrollView } from 'react-native'

const List= ({lista}) => {
    return (
        <ScrollView>
        {lista.map(el =>(
          <View style={styles.lista} key={el.id}>
            <Text style={styles.textoLista} >{el.nombre}</Text>
          </View>
        ))}
      </ScrollView>
    )
}
const styles=StyleSheet.create({
  lista:{
    marginHorizontal:10,
    paddingVertical:25,
    borderBottomWidth:1,
    borderBottomColor:'#ccc',
  },
  textoLista:{
    fontSize:20,
  },
})
export default List;
